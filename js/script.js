$(document).ready(function() {

    // ketikan source code yang ada di modul
    $.getJSON('json/pizza.json', function(data){
       var menu = data.menu;
       $.each(menu,function(i,data){
   
    $('#daftar-menu').append($('<div class="col-sm-4"><div class="card"><img class="card-img-top" src="'+ data.gambar 
    +'"><div class="card-body"><h4 class="card-title">'+ data.nama +'</h4><p class="card-text">'+ data.deskripsi +'</p><h3 class="card-title">'
    + rupiah(data.harga) +'</h3><a href="#" class="btn btn-primary">Pesan Sekarang!</a></div></div></div>'));
         });
    });
});

$('.nav-link').on('click', function(){
    var kategori = $(this).data('kategori');
    var judul = $(this).text();
    
    $('h1').html(judul);
    $('.nav-link').removeClass('active');
    $(this).addClass('active');

    $.getJSON('json/pizza.json', function(data) {
        var menuAll = data.menu;

        var menu = $.grep(menuAll, function(data) {
            return data.kategori == kategori;
        });

    if (menu.length == 0) {
        menu =  menuAll;
    }

    var hasil = '';
    $.each(menu, function(i,data) {
        hasil += '<div class="col-sm-4"><div class="card"><img class="card-img-top" src="'+ data.gambar +'"><div class="card-body"><h4 class="card-title">'+ data.nama +'</h4><p class="card-text">'+ data.deskripsi +'</p><h3 class="card-title">'+ rupiah(data.harga) +'</h3><a href="#" class="btn btn-primary">Pesan Sekarang!</a></div></div></div>';
    });
    $('#daftar-menu').html(hasil);
});
    return false;
});