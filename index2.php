<!doctype html>
<html lang="en">
  <head>
    <title>RekWeb Hut | Awesome Pizza for Awesome Programmers</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/style.css">
  </head>
  <body>

    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
      <div class="container">
        <a class="navbar-brand" href="#">
          <img src="img/logo2.png" height="40" class="d-inline-block align-top" alt="">
        </a>

        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>

       <!-- ketikan source yang ada di modul -->
       <div class="collapse navbar-collapse" id="navbarNav">
        <ul class="navbar-nav">
          <li class="nav-item">
            <a href="#" class="nav-link active" data-kategori="all">All Menu</a>
          </li>
          <li class="nav-item">
            <a href="#" class="nav-link " data-kategori="pizza">Pizza</a>
          </li>
          <li class="nav-item">
            <a href="#" class="nav-link" data-kategori="pasta">Pasta</a>
          </li>
          <li class="nav-item">
            <a href="#" class="nav-link" data-kategori="nasi">Nasi</a>
          </li>
          <li class="nav-item">
            <a href="#" class="nav-link" data-kategori="minuman">Minuman</a>
          </li>
        </ul>
       </div>
      </div>
    </nav>

    <div class="container">
      
        <div class="row">
          <div class="col-sm ">
            <h1>All Menu</h1>
          </div>
        </div>

        <div class="row" id="daftar-menu"></div>

    </div>


    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="js/jquery-3.2.1.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
    <script src="js/bootstrap.js"></script>
    <script src="js/functions.js"></script>
    <script src="js/script.js"></script>
  </body>
</html>