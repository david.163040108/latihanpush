<!-- Ketikan source code yang ada di modul -->
<?php
  require 'helper/functions.php';

  $contents = file_get_contents('json/pizza.json');

  $contents = utf8_encode($contents);

  $results  = json_decode($contents, true);

  $menu = $results["menu"];
  if (isset($_GET["kategori"]) && !empty($_GET["kategori"])) {
    $hasil = [];
    foreach ($menu as $key ) {
      if ($key["kategori"] == $_GET["kategori"]) {
        $hasil[] =$key;
      }
    }
    $menu = $hasil;
  }
?>
<!doctype html>
<html lang="en">
  <head>
    <title>RekWeb Hut | Awesome Pizza for Awesome Programmers</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/style.css">
  </head>
  <body>

    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
      <div class="container">
        <a class="navbar-brand" href="#">
          <img src="img/logo2.png" height="40" class="d-inline-block align-top" alt="">
        </a>

        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>

        <!-- Ketikan source code yang ada di modul -->
        <div class="collpase navbar-collapse" id="navbarNav">
          <ul class="navbar-nav">
              <li class="nav-item <?= (!isset($_GET['kategori']) || empty($_GET["kategori"])) ? "active" : ""?>">
                <a class="nav-link" href="?">All Menu <span class="sr-only">(current)</span></a>
              </li>

              <li class="nav-item <?= (!isset($_GET['kategori']) && $_GET["kategori"] == "pizza") ? "active" : ""?>">
                <a class="nav-link" href="?kategori=pizza">Pizza <span class="sr-only">(current)</span></a>
              </li>

              <li class="nav-item <?= (!isset($_GET['kategori']) && $_GET["kategori"] == "pasta") ? "active" : ""?>">
                <a class="nav-link" href="?kategori=pasta">Pasta <span class="sr-only">(current)</span></a>
              </li>
              <li class="nav-item <?= (!isset($_GET['kategori']) && $_GET["kategori"] == "nasi") ? "active" : ""?>">
                <a class="nav-link" href="?kategori=nasi">Nasi <span class="sr-only">(current)</span></a>
              </li>
              <li class="nav-item <?= (!isset($_GET['kategori']) && $_GET["kategori"] == "minuman") ? "active" : ""?>">
                <a class="nav-link" href="?kategori=minuman">Minuman <span class="sr-only">(current)</span></a>
              </li>

          </ul>
        </div>
      </div>
    </nav>

    <div class="container">
      
        <div class="row">
          <div class="col-sm ">
            <h1>Pizza </h1>
          </div>
        </div>

        <div class="row" id="daftar-menu">
          
          <?php foreach( $menu as $row ) : ?>
            <div class="col-sm-4">
              <div class="card">
                <img class="card-img-top" src="<?= $row["gambar"]; ?>" alt="Card image cap">
                <div class="card-body">
                  <h4 class="card-title"><?= $row["nama"]; ?></h4>
                  <p class="card-text"><?= $row["deskripsi"]; ?></p>
                  <h3 class="card-title"><?= rupiah($row["harga"]); ?></h3>
                  <a href="#" class="btn btn-primary">Pesan Sekarang!</a>
                </div>
              </div>
            </div>
          <?php endforeach; ?>

        </div>

    </div>


    

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="js/jquery-3.2.1.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
    <script src="js/bootstrap.js"></script>
  </body>
</html>